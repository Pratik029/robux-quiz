//
//  Quiz.swift
//  Robux Quiz
//
//  Created by Nouman on 2/19/19.
//  Copyright © 2019 faari27@gmail.com. All rights reserved.
//

import Foundation

struct Quiz : Codable {
    let name : String?
    let questions : [Questions]?
    
    enum CodingKeys: String, CodingKey {
        
        case name = "name"
        case questions = "questions"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        questions = try values.decodeIfPresent([Questions].self, forKey: .questions)
    }
    
}

struct Questions : Codable {
    let question_id : Int?
    let question : String?
    var options : [String] = []
    let correct_option : String?
    
    enum CodingKeys: String, CodingKey {
        
        case question_id = "question_id"
        case question = "question"
        case options = "options"
        case correct_option = "correct_option"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        question_id = try values.decodeIfPresent(Int.self, forKey: .question_id)
        question = try values.decodeIfPresent(String.self, forKey: .question)
        options = try values.decodeIfPresent([String].self, forKey: .options) ?? []
        correct_option = try values.decodeIfPresent(String.self, forKey: .correct_option)
    }
    
}

//struct Options : Codable {
//    var option_a : String = ""
//    var option_b : String = ""
//    var option_c : String = ""
//
//    enum CodingKeys: String, CodingKey {
//
//        case option_a = "option_a"
//        case option_b = "option_b"
//        case option_c = "option_c"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        option_a = try values.decodeIfPresent(String.self, forKey: .option_a) ?? ""
//        option_b = try values.decodeIfPresent(String.self, forKey: .option_b) ?? ""
//        option_c = try values.decodeIfPresent(String.self, forKey: .option_c) ?? ""
//    }
//
//}
//
//
struct QuizBase : Codable {
    let quiz : [Quiz]?

    enum CodingKeys: String, CodingKey {

        case quiz = "Quiz"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        quiz = try values.decodeIfPresent([Quiz].self, forKey: .quiz)
    }

}
