//
//  Constants.swift
//  Rezeptrechner
//
//  Created by Furqan on 10/28/18.
//  Copyright © 2018 faari27@gmail.com. All rights reserved.
//

import Foundation
import UIKit

func getAllQuizWith() -> [Quiz]? {
    
    let path = Bundle.main.path(forResource: "quiz", ofType: "json")
    let url = URL(fileURLWithPath: path!)
    
    do {
        let data = try Data(contentsOf: url)
        
        let jsonDecoder = JSONDecoder()
        let mainArray = try jsonDecoder.decode(QuizBase.self, from: data)
        
        // saving today quote to todayQuote string which is located in Constant.swift file
        return mainArray.quiz
    }
        
    catch {}
    
    return nil
}

public func getPrimeryColor() -> UIColor {
    return UIColor(0x31BB68)
}



// ADMob Stuff

let applicationID = "ca-app-pub-6610497664170714/1116550407"
