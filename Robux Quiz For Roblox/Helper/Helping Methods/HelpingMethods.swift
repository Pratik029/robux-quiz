
//
//  HelpingMethods.swift
//  AkuranaToday
//
//  Created by Nouman on 2/9/19.
//  Copyright © 2019 faari27@gmail.com. All rights reserved.
//

import Foundation
import UIKit


func getDescriptionSetting(description: String) -> NSAttributedString {
    
    let descriptionInfo = NSMutableAttributedString(string: description)
    
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 5
    descriptionInfo.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, descriptionInfo.length))
    return descriptionInfo
}

func noResultFoundCell(tableView: UITableView) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "NoResultFound") else { return UITableViewCell()}
    
    return cell
}
