//
//  Utils.swift
//  Rezeptrechner
//
//  Created by Furqan on 10/24/18.
//  Copyright © 2018 faari27@gmail.com. All rights reserved.
//

import UIKit

/*Utility functions used in the entire app*/
class Utils {
    
    static func switchToOnboarding() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let rootViewController = UIStoryboard(name: "OnBoarding", bundle: nil).instantiateInitialViewController() {
            appDelegate.window?.rootViewController = rootViewController
        }
    }
    
    static func switchToMain() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() {
            appDelegate.window?.rootViewController = rootViewController
        }
    }
    
}

