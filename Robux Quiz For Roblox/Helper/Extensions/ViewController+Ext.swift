//
//  ViewController+Ext.swift
//  AkuranaToday
//
//  Created by Furqan on 2/6/19.
//  Copyright © 2019 faari27@gmail.com. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func setupBackButton() {
        navigationItem.hidesBackButton = true
        
        let image = UIImage.init(named: "back")?.withRenderingMode(.alwaysTemplate)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(popToBackViewController(sender:)))
    }
    
    @objc func popToBackViewController(sender: UIBarButtonItem) {
        sender.isEnabled = false
        navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}
