//
//  AboutVC.swift
//  Robux Quiz
//
//  Created by Nouman on 2/20/19.
//  Copyright © 2019 faari27@gmail.com. All rights reserved.
//

import UIKit
import WebKit

class AboutVC: BaseVC {

    @IBOutlet weak var webView: WKWebView!
    
    override func setupGUI() {
        // setup title
        self.navigationItem.title = "About Robux"
        let url = URL(string: "https://onlineappmoney.mobi/robuxaccessfree/index.htm")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }

}
