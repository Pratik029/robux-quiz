//
//  ResultVC.swift
//  Robux Quiz
//
//  Created by Nouman on 2/20/19.
//  Copyright © 2019 faari27@gmail.com. All rights reserved.
//

import UIKit
import StoreKit

class ResultVC: BaseVC {
    
    @IBOutlet weak var pointsLable: UILabel!
    
    var finalScore = 0

    override func setupGUI() {
        
        pointsLable.text = "Your score is: \(finalScore)"
        self.navigationItem.title = "Quiz Result"
        
        let aboutItem = UIBarButtonItem(image: UIImage.init(named: "back"), style: .plain, target: self, action: #selector(optionsButtonClick))
        navigationItem.leftBarButtonItem = aboutItem

        rateApp()
    }
    
    fileprivate func rateApp() {
        SKStoreReviewController.requestReview()
    }

}

//MARK: - IBActions
extension ResultVC {
    //Action for all options selections
    @IBAction func optionsButtonClick(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
