//
//  SplashVC.swift
//  Rezeptrechner
//
//  Created by Furqan on 10/24/18.
//  Copyright © 2018 faari27@gmail.com. All rights reserved.
//

import UIKit

class SplashVC: BaseVC {
    
    override func setupGUI() {
        
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            let homeVC: HomeVC = self.getViewController(vcName: "HomeVC")
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
}

