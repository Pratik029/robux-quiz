//
//  CategoryVC.swift
//  Robux Quiz
//
//  Created by Nouman on 2/15/19.
//  Copyright © 2019 faari27@gmail.com. All rights reserved.
//

import UIKit
import SimpleAnimation

class CategoryVC: BaseVC {
    
    @IBOutlet weak var categoriesView1: UIView!
    @IBOutlet weak var categoriesView2: UIView!
    @IBOutlet weak var categoriesView3: UIView!
    
    override func setupGUI() {
        self.title = "Category"
        shakeView()
    }
  
}


//MARK: - IBAction
extension CategoryVC {
    
    @IBAction func categoryActionButton(_ sender: UIButton) {
        
        let questionsVC: QuestionsVC = getViewController(vcName: "QuestionsVC")
        self.navigationController?.pushViewController(questionsVC, animated: true)
        
        if sender.tag == 0 {
            questionsVC.categoryIndex = 0
        } else if sender.tag == 1 {
            questionsVC.categoryIndex = 1
        } else {
            questionsVC.categoryIndex = 2
        }
    }
    
}

//MARK: - Animations Methods
extension CategoryVC {
    
   fileprivate func shakeView() {
        self.categoriesView1.bounceIn(from: .left, x: 0, duration: 1, delay: 0.1)
        self.categoriesView2.bounceIn(from: .left, x: 0, duration: 1, delay: 0.2)
        self.categoriesView3.bounceIn(from: .left, x: 0, duration: 1, delay: 0.3)
    }
    
}
