//
//  ViewController.swift
//  Robux Quiz
//
//  Created by Nouman on 2/15/19.
//  Copyright © 2019 faari27@gmail.com. All rights reserved.
//

import UIKit
import SimpleAnimation
import GoogleMobileAds

class HomeVC: BaseVC {

    var timer = Timer()
    var supportButton = UIButton()
    
    //ADMob stuff
    var bannerView: GADBannerView!
    var interstitial: GADInterstitial!

    override func setupGUI() {
        
        // Admob stuff
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-6610497664170714/1116550407")
        interstitial.load(GADRequest())

        setupNavAndTabBar()
        
        self.timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: (#selector(animateRightIcon)), userInfo: nil, repeats: true)
    }
    
    override func updateGUI() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        interstitial.present(fromRootViewController: self)
        print("Ad showing")
        setupNavAndTabBar()
    }
    
    @objc func animateRightIcon() {
        supportButton.hop()
    }
}

//MARK: - IBAction
extension HomeVC {
    
    @objc func settingButtonAction() {
        let settingVC: SettingVC = getViewController(vcName: "SettingVC")
        self.navigationController?.pushViewController(settingVC, animated: true)    }
    
    @objc func aboutButtonAction() {
        let aboutVC: AboutVC = getViewController(vcName: "AboutVC")
        self.navigationController?.pushViewController(aboutVC, animated: true)
    }
    
    @IBAction func playQuizActionButton(_ sender: UIButton) {

        if interstitial.isReady {
            
            interstitial.present(fromRootViewController: self)
            print("Ad showing")
            
        } else {
            
            print("Ad wasn't ready")
            let categoryVc: CategoryVC = getViewController(vcName: "CategoryVC")
            self.navigationController?.pushViewController(categoryVc, animated: true)
            
            interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
            interstitial.load(GADRequest())

        }
    }
}


//MARK: - Class Methods
extension HomeVC {
    
    fileprivate func setupNavAndTabBar() {
        
        // setup title and its color
        self.navigationItem.title = "Quiz"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let attributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 50)!]
        UINavigationBar.appearance().titleTextAttributes = attributes
        
        // changing nav bar color
//        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        
//        let aboutItem = UIBarButtonItem(image: UIImage.init(named: "setting"), style: .plain, target: self, action: #selector(settingButtonAction))
//        navigationItem.leftBarButtonItem = aboutItem
        let supportIcon  = UIImage(named: "about")
        supportButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        supportButton.setBackgroundImage(supportIcon, for: .normal)
        supportButton.addTarget(self, action:  #selector(aboutButtonAction), for: .touchUpInside)
    
        let support = UIBarButtonItem(customView: supportButton)
        navigationItem.rightBarButtonItem = support

//        let settingItem = UIBarButtonItem(image: UIImage.init(named: "about"), style: .plain, target: self, action: #selector(aboutButtonAction))
//
//        navigationItem.rightBarButtonItem = settingItem

    }
    
}
