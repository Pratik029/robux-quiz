//
//  BaseVC.swift
//  ShopWellV2
//
//  Created by Furqan on 10/04/2018.
//  Copyright © 2018 faari27@gmail.com. All rights reserved.
//

import UIKit

class BaseVC: UIViewController{

    /*One Time code setup*/
    func setupGUI() -> Void {
//        self.navigationController?.navigationBar.isHidden = true
    }
    
    /*Code to reload the view. No One time code should be part of it*/
    func updateGUI() -> Void {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = getPrimeryColor()
        setupGUI()
        updateGUI()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnPressed(sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title: String, message: String) -> Void {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getViewController<T: UIViewController>(vcName: String) -> T {
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: vcName) as! T
        return vc
        
    }

}







