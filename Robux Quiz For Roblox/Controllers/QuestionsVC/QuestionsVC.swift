//
//  QuestionsVC.swift
//  Robux Quiz
//
//  Created by Nouman on 2/19/19.
//  Copyright © 2019 faari27@gmail.com. All rights reserved.
//

import UIKit
import SRCountdownTimer
import SimpleAnimation

class QuestionsVC: BaseVC {
    
    @IBOutlet weak var optionsButtonOutlete: UIButton!
    //UIView Outlets
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var countdownTimer: SRCountdownTimer!
    @IBOutlet weak var option1: UIView!
    @IBOutlet weak var option2: UIView!
    @IBOutlet weak var option3: UIView!
    @IBOutlet weak var question: UIView!

    //UILabel Outlets
    @IBOutlet weak var questonNumberLable: UILabel!
    @IBOutlet weak var pointsLable: UILabel!
    @IBOutlet weak var questionLable: UILabel!
    
    @IBOutlet weak var optionALable: UILabel!
    @IBOutlet weak var optionBLable: UILabel!
    @IBOutlet weak var optionCLable: UILabel!
    
    
    
    //MARK: - Class Properties
    var categoryIndex: Int = 0
    var allQuizArray: [Quiz] = []
    var questionArray: [Questions] = []
    var questionIndex = 0
    var totalQuestion = 0
    var points = 0
    

    
    override func setupGUI() {


        
        countdownTimer.delegate = self
        setupNavAndTabBar()
        
        //Array of All Quiz
        allQuizArray = getAllQuizWith() ?? []
        
        //question from single quiz
        questionArray = allQuizArray[categoryIndex].questions ?? []
        
        totalQuestion = questionArray.count
        
        //set values to labels
        quizQuestionWith(index: questionIndex)
      
    }
    
    override func updateGUI() {
        
    }
        
}

//MARK: - IBActions
extension QuestionsVC {
    
    //Action for all options selections
    @IBAction func optionsButtonClick(_ sender: UIButton) {
        
        changeColorStateAt(index: sender.tag)

        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.quizQuestionWith(index: self.questionIndex)
        }
        questionIndex += 1

    }
}

//MARK: - Class Methods
extension QuestionsVC {
    
    fileprivate func setupNavAndTabBar() {
        
        // setup title and its color
        self.navigationItem.title = "Questions"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let attributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 50)!]
        UINavigationBar.appearance().titleTextAttributes = attributes
        
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        
        let skipItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(skipButtonAction))
        
        navigationItem.rightBarButtonItem = skipItem
        
    }
    
    fileprivate func quizQuestionWith(index: Int) {
        
        if index <= totalQuestion - 1{
            self.view.isUserInteractionEnabled = true
            //Shake on change Question
            shakeView()
            
            //Start Timer
            startTimer()
            
            //reset view Background color to white
            resetColorToDefault()
            
            //set Json data
            questonNumberLable.text = "\(index + 1)/\(totalQuestion)"
            questionLable.text = questionArray[index].question
            optionALable.text = questionArray[index].options[0]
            optionBLable.text = questionArray[index].options[1]
            
            if categoryIndex == 2 {
                option3.isHidden = true
            } else {
                optionCLable.text = questionArray[index].options[2]
            }
            
        } else {
            let resultVC: ResultVC = getViewController(vcName: "ResultVC")
            resultVC.finalScore = points
            self.navigationController?.pushViewController(resultVC, animated: true)
        }
    }
    
    func changeColorStateAt(index: Int) {
        
        self.view.isUserInteractionEnabled = false
        //check crrect option
        let correctOption = questionArray[questionIndex].correct_option
        
        //get selected Lable text by selected index
        let selectedOption = questionArray[questionIndex].options[index]
        
        // if both are equal change color to green
        if correctOption == selectedOption {
            points += 10
            pointsLable.text = "\(points)"
            if let selectedView: UIView = view.viewWithTag(index + 5) {
                selectedView.backgroundColor = .green
            }
        } else {
            points -= 5
            pointsLable.text = "\(points)"
            
            //shake when user click on wrong option
            onWrongClickShake()
            
            // if not equal then check index or correct option
            let correctAnswer: Int = questionArray[questionIndex].options.index(of: correctOption!) ?? 0
            
            //give color red to selected wrong option
            if let selectedView: UIView = view.viewWithTag(index + 5) {
                selectedView.backgroundColor = .red
            }
            
            // give color green to correct option
            if let selectedView: UIView = view.viewWithTag(correctAnswer + 5) {
                selectedView.backgroundColor = .green
            }
        }
    }
    
    func resetColorToDefault() {
        
        for i in 5...7 {
            
            // give color green to correct option
            if let selectedView: UIView = view.viewWithTag(i) {
                selectedView.backgroundColor = .white
            }
        }
    }
   
    
}

//MARK: - Class Methods
extension QuestionsVC {
    
    @objc func skipButtonAction() {
        questionIndex += 1
        quizQuestionWith(index: questionIndex)
    }
    
}

//MARK: - SRCountdownTimer Delegate
extension QuestionsVC: SRCountdownTimerDelegate {
    @objc func timerDidUpdateCounterValue(newValue: Int) {
        if newValue < 8 {
            countdownTimer.lineColor = UIColor(0xfff59d)
            countdownTimer.hop()
        }
        
        if newValue < 5 {
            countdownTimer.lineColor = UIColor(0xe57373)
            countdownTimer.hop()
        }
    }

    @objc func timerDidEnd() {
        skipButtonAction()
    }
}

//MARK: - Animations Methods
extension QuestionsVC {
    
    func startTimer() {
        
        countdownTimer.labelFont = UIFont(name: "HelveticaNeue-Light", size: 50.0)
        countdownTimer.labelTextColor = UIColor.white
        countdownTimer.timerFinishingText = "End"
        countdownTimer.lineWidth = 4
        countdownTimer.lineColor = .green
        countdownTimer.start(beginingValue: 15, interval: 1)
    }
    
    fileprivate func shakeView() {
        question.popIn()
        self.option1.bounceIn(from: .left, x: 0, duration: 1, delay: 0.1)
        self.option2.bounceIn(from: .left, x: 0, duration: 1, delay: 0.2)
        self.option3.bounceIn(from: .left, x: 0, duration: 1, delay: 0.3)
    }
    
    func onWrongClickShake() {
        self.option1.shake(toward: .left)
        self.option2.shake()
        self.option3.shake()
    }
}
